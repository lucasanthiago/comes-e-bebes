<?php
// Template Name: pagina inicial
?>
<div id="opacity"></div>
<?php get_header(); ?>
    <main>
        <div class="LPAbout">
            <h1 class="LPTitle">Comes&Bebes</h1>
            <p class="LPDescription">O restaurante para todas as fomes</p>
        </div>
        <div class="LPContainer">
            <h1 class="ContainerTitle">CONHEÇA NOSSA LOJA</h1>
            <div class="AllCategories">
            <h3 class="AllCategoriesTitle">Tipos de pratos principais</h3>
                <div class="EachCategory">
                    <?php wp_nav_menu(array('theme_location' => 'categories-menu')); ?>
                </div>
            </div>
        </div>
        <div class="PratosDoDia">
            <h3 class="DoDiaTitle">Pratos do dia de hoje</h3>
            <?php
            setlocale(LC_TIME, 'pt_BR', 'pt_BR.UTF-8', 'pt_BR.utf-8', 'portuguese');
            date_default_timezone_set('America/Sao_Paulo');
            $dia =  mb_strtoupper(utf8_encode(strftime('%A', strtotime('today'))), mb_internal_encoding());
            echo $dia;
            ?>
            <div class="DoDiaPratos">
                <ul class="products">
                    <?php 
                    $args = array('post_type'=>'product','posts_per_page'=>4,'product_tag'=>$dia);
                    $loop = new WP_Query($args);
                    
                    while($loop->have_posts()){
                        $loop->the_post();                      
                        global $post;
                        wc_get_template_part('content','product');  
                    }

                    ?>
                </ul>
            </div>


           
          <a class="goToProductsList" href="/shop"><button class="OtherOptions" type="button">Veja outras opções</button></a>
        </div>
        
        <div class="LojaFisica">
            <h1 class="LFTitle">VISITE NOSSA LOJA FÍSICA</h1>
            <div class="LFContent">
                <div class="Mapa">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14700.922543994884!2d-43.1316667!3d-22.9048625!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9e97773b91b93bba!2sUniversidade%20Federal%20Fluminense%20-%20Campus%20Praia%20Vermelha!5e0!3m2!1spt-BR!2sbr!4v1660446288053!5m2!1spt-BR!2sbr" width="400" height="220" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    <div class="LFAdress">
                        <img class="RestauranteImg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/garfo e faca.svg">
                        <p class="ResutauranteAdress">Rua Passos da Patria, 152, Rio de Janeiro, Brasil</p>
                    </div>
                    <div class="LFContato">
                        <img class="ContatoImg" src="<?php echo get_stylesheet_directory_uri() ?>/assets/telefone.svg">
                        <p class="ContatoNumber">(21)26295368</p>
                    </div>
                </div>
                <div class="slides">
                    <ul class="slider">
                        <li>
                            <input type="radio" id="slide1" name="slide" checked>
                            <label for="slide1"></label>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/restaurante.png" />
                        </li>
                        <li>
                            <input type="radio" id="slide2" name="slide">
                            <label for="slide2"></label>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/coxinha-de-jaca.jpg" />
                        </li>
                        <li>
                            <input type="radio" id="slide3" name="slide">
                            <label for="slide3"></label>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/lasanha bolonhesa.jpeg" />
                        </li>
                        <li>
                            <input type="radio" id="slide4" name="slide">
                            <label for="slide4"></label>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/ramen.jpg" />
                        </li>
                        </li>
                    </ul>
                </div>
            </div> 
        </div>

        
    </main>
<?php get_footer(); ?>