    <footer class="rodape">
        <p class="copyrigth">© Copyright 2021 - Desenvolvido por IL?"Info_loucos"?</p>
    </footer>

    <script type="text/javascript">
        document.getElementById("close-nav-btn").addEventListener("click", () => closeNav())
        document.getElementById("mini-cart").addEventListener("click", () => openNav())

        let main = document.getElementsByTagName('main')[0]
        let opacity = document.getElementById('opacity')
    

        function openNav() {
            document.getElementById("side-nav").style.width = "max(200px, 30%)";
            opacity.style.display = 'block'
        }
        
        function closeNav() {
            document.getElementById("side-nav").style.width = "0";
            opacity.style.display = 'none'
        }
    </script>
    <?php wp_footer(); ?>
</body>
</html>