<?php

    @require 'inc/remove-display-name.php';
    @require 'inc/orders-display.php';
    @require 'inc/archive-product-menu.php';
    @require 'inc/create-header-menu.php';


    add_filter( 'woocommerce_variable_sale_price_html', 'wpglorify_variation_price_format', 10, 2 );
    add_filter( 'woocommerce_variable_price_html', 'wpglorify_variation_price_format', 10, 2 );
    
    function wpglorify_variation_price_format( $price, $product ) {

        $prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
        $price = $prices[0] !== $prices[1] ? sprintf( __( '%1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
        
        $prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
        sort( $prices );
        $saleprice = $prices[0] !== $prices[1] ? sprintf( __( '%1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
        
        if ( $price !== $saleprice ) {
        $price = '<del>' . $saleprice . $product->get_price_suffix() . '</del> <ins>' . $price . $product->get_price_suffix() . '</ins>';
        }
        return $price;
    }

    add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

    function new_loop_shop_per_page( $cols ) {
        $cols = 8;
        return $cols;
    }

    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

    function register_menus(){
        register_nav_menus(array(
            'header-menu' => 'Menu Header',
            'account-menu' => 'Menu Usuário',
            'categories-menu' => 'Menu Categorias'
        ));
    }

    function add_woocommerce_support(){
        add_theme_support('woocommerce');
    }

    add_action('init', 'register_menus');
    add_action('after_setup_theme', 'add_woocommerce_support');

    function adicionar_esilo(){
        wp_register_style('style',get_template_directory_uri().'/style.css',[],'1.0.0');
        wp_enqueue_style( 'style', get_template_directory_uri().'/style.css',[],'1.0.0');
    }

    add_action( 'wp_enqueue_scripts', 'adicionar_esilo');

    
    
    //add_filter("woocommerce_default_address_fields","adicionar_field");


    function change_wp_nav_menu_objects($items){
        global $template;
        
       

        if (basename($template) == 'page.php'){
            return $items;

        }

        foreach($items as $item){

        $thumbnail_id = get_term_meta( $item->object_id, 'thumbnail_id', true ); 

        $image = wp_get_attachment_url( $thumbnail_id ); 

        $css =  'background-image: url('. $image .')';

        $item->title = 
        '<div class="CategoryContent" style="'. $css .'; background-position: center; background-size:cover;">        
            <div class="CategoryTitle">
                <p>'. mb_strtoupper($item->title ,mb_internal_encoding()) . '</p>
            </div>
        </div>';
             
        }
        return $items;
}
    add_filter('wp_nav_menu_objects', "change_wp_nav_menu_objects");

    function remove_shipping_fields($address_shipping_fields){
        unset($address_shipping_fields['company']);
        unset($address_shipping_fields['state']);
        unset($address_shipping_fields['address_2']);
        $address_shipping_fields['address_1']['label'] = 'Logradouro';
        $address_shipping_fields['address_1']['placeholder'] = 'Rua e número';
        $address_shipping_fields['city']['priority'] = 80;
        $address_shipping_fields['city']['class'] = ['form-row-last'];
        $address_shipping_fields['city']['placeholder'] = 'Sua cidade';
        $address_shipping_fields['postcode']['priority'] = 30;
        $address_shipping_fields['postcode']['placeholder'] = 'Digite seu CEP';
        $address_shipping_fields['first_name']['placeholder'] = 'Digite seu nome';
        $address_shipping_fields['last_name']['placeholder'] = 'Digite seu sobrenome';


       
        return $address_shipping_fields;

    }

    add_filter('woocommerce_default_address_fields','remove_shipping_fields');

    function add_shipping_fields($address_shipping_fields){
        $address_shipping_fields['bairro'] = array(
            'label' => 'Bairro',
            'required' => true,
            'class' => array('form-row-first'),
            'priority' => 70,
            'placeholder' => 'Seu bairro',
        );
        $address_shipping_fields['complemento'] = array(
            'label' => 'Complemento',
            'required' => true,
            'class' => array('form-row-wide'),
            'priority' => 50,
            'placeholder' => 'Complemento do seu endereço',
        );
        return $address_shipping_fields;
    }

    add_filter('woocommerce_default_address_fields','add_shipping_fields');

    function change_shipping_title($title){
        return 'Novo / Edite seu endereço';
    }

    add_filter( 'woocommerce_my_account_edit_address_title','change_shipping_title');

    function change_address_description($description){
        return 'Os endereços a seguir serão usados na página de finalizar pedido como endereços padrões, mas é possível modificá-los durante a finalização do pedido.';
    }

    add_filter('woocommerce_my_account_my_address_description','change_address_description');


    function custom_address_formats( $formats ) {
    $formats[ 'default' ]  = "
    <div class='all-address-container-enderecos'>
        <div class='all-address-name'> {name} </div>
        <div class='all-address-cep'>{postcode} </div>
        <div class='all-address-endereco'>{address_1} \n {city} </div>   
    </div>
    ";
    return $formats;
}
add_filter('woocommerce_localisation_address_formats', 'custom_address_formats');

    function woocommerce_single_variation() {
		echo '<h2 class="single-product-price woocommerce-variation single_variation"></h2>';
	}
