<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>
	<?php
	do_action( 'woocommerce_before_shop_loop_item' );
	global $product;
	$product_img_id = $product->image_id;

	$image = wp_get_attachment_url( $product_img_id );
	$image_size = getimagesize($image);
	?>
	<div class="product-content" style="width:240px; height:373px;">
		<img class="product-img" src="<?= $image; ?>">
		<div class="product-info">
			<p class="product-name"><?= $product->name; ?></p>
			<div class="product-price-cart">
				<p class="product-price"> <?= $product->get_price_html() ?></p>
				<a href='<?= $product->add_to_cart_url(); ?>'>
					<button class="product-add-to-cart">
						<img class="add-to-cart-icon" src="<?= get_template_directory_uri(); ?>/assets/add-to-cart-icon.svg ">
					</button>
				</a>
			</div>
		</div>
	</div>
	
	<?php
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li>